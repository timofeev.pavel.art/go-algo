package linkedlist

const (
	emptyList  = "list is empty"
	notFound   = "data not found"
	outOfRange = "index out of range"
)

type SingleLister[T comparable] interface {
	Len() int
	Contains(data T) bool
	Add(data T)
	Remove(data T) error
	Shift() (T, error)
	Pop() (T, error)
}

type singleListNode[T comparable] struct {
	data T
	next *singleListNode[T]
}

type DoubleLister[T comparable] interface {
	Next()
	GetCurr() (T, error)
	GetHead() (T, error)
	GetTail() (T, error)
	Prev()
	MoveToHead()
	MoveToTail()
	MoveTo(idx int) error
	AddHead(data T)
	AddCurr(data T)
	AddToIdx(id int, data T) error
	AddTail(data T)
	Remove(data T) error
	RemoveByIdx(id int) error
	RemoveHead() error
	RemoveCurr() error
	RemoveTail() error
	Contains(data T) bool
	GetArray() ([]T, error)
	GetArrayReverse() ([]T, error)
	Len() int
}

type doubleListNode[T comparable] struct {
	data T
	next *doubleListNode[T]
	prev *doubleListNode[T]
}
