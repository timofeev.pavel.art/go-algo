package linkedlist

import (
	"reflect"
	"testing"
)

func TestDoubleList_Next_GetCurr(t *testing.T) {
	tests := []struct {
		name  string
		list  DoubleLister[int]
		items []int
		shift int
		want  int
	}{
		{
			name:  "empty list",
			list:  NewDoubleList[int](),
			items: nil,
			shift: 0,
			want:  0,
		},
		{
			name:  "[1 2 3] array, 0 shift",
			list:  NewDoubleList[int](),
			items: []int{1, 2, 3},
			shift: 0,
			want:  1,
		},
		{
			name:  "[1 2 3] array, 1 shift",
			list:  NewDoubleList[int](),
			items: []int{1, 2, 3},
			shift: 1,
			want:  2,
		},
		{
			name:  "[1 2 3] array, 2 shift",
			list:  NewDoubleList[int](),
			items: []int{1, 2, 3},
			shift: 2,
			want:  3,
		},
		{
			name:  "[1 2 3] array, 5 shift",
			list:  NewDoubleList[int](),
			items: []int{1, 2, 3},
			shift: 5,
			want:  3,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				tt.list.AddTail(item)
			}

			for i := 0; i != tt.shift; i++ {
				tt.list.Next()
			}

			if got, _ := tt.list.GetCurr(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DoubleList.Next() = %d, want %d", got, tt.want)
			}
		})
	}
}

func TestDoubleList_Prev_GetCurr(t *testing.T) {
	tests := []struct {
		name  string
		list  DoubleLister[int]
		items []int
		shift int
		want  int
	}{
		{
			name:  "empty list",
			list:  NewDoubleList[int](),
			items: nil,
			shift: 0,
			want:  0,
		},
		{
			name:  "[1 2 3] array, 0 shift",
			list:  NewDoubleList[int](),
			items: []int{1, 2, 3},
			shift: 0,
			want:  3,
		},
		{
			name:  "[1 2 3] array, 1 shift",
			list:  NewDoubleList[int](),
			items: []int{1, 2, 3},
			shift: 1,
			want:  2,
		},
		{
			name:  "[1 2 3] array, 2 shift",
			list:  NewDoubleList[int](),
			items: []int{1, 2, 3},
			shift: 2,
			want:  1,
		},
		{
			name:  "[1 2 3] array, 5 shift",
			list:  NewDoubleList[int](),
			items: []int{1, 2, 3},
			shift: 5,
			want:  1,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				tt.list.AddTail(item)
			}

			tt.list.MoveToTail()

			for i := 0; i != tt.shift; i++ {
				tt.list.Prev()
			}

			if got, _ := tt.list.GetCurr(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DoubleList.Prev() = %d, want %d", got, tt.want)
			}
		})
	}
}

func TestDoubleList_MoveTo(t *testing.T) {
	tests := []struct {
		name    string
		list    DoubleLister[int]
		items   []int
		moveto  int
		want    int
		wantErr bool
	}{
		{
			name:    "empty list",
			list:    NewDoubleList[int](),
			items:   nil,
			moveto:  1,
			want:    0,
			wantErr: true,
		},
		{
			name:    "[1] array, move to 0",
			list:    NewDoubleList[int](),
			items:   []int{1},
			moveto:  0,
			want:    1,
			wantErr: false,
		},
		{
			name:    "[1 2 3 4 5] array, move to 0",
			list:    NewDoubleList[int](),
			items:   []int{1, 2, 3, 4, 5},
			moveto:  0,
			want:    1,
			wantErr: false,
		},
		{
			name:    "[1 2 3 4 5] array, move to 1",
			list:    NewDoubleList[int](),
			items:   []int{1, 2, 3, 4, 5},
			moveto:  1,
			want:    2,
			wantErr: false,
		},
		{
			name:    "[1 2 3 4 5] array, move to 2",
			list:    NewDoubleList[int](),
			items:   []int{1, 2, 3, 4, 5},
			moveto:  2,
			want:    3,
			wantErr: false,
		},
		{
			name:    "[1 2 3 4 5] array, move to 3",
			list:    NewDoubleList[int](),
			items:   []int{1, 2, 3, 4, 5},
			moveto:  3,
			want:    4,
			wantErr: false,
		},
		{
			name:    "[1 2 3 4 5] array, move to 4",
			list:    NewDoubleList[int](),
			items:   []int{1, 2, 3, 4, 5},
			moveto:  4,
			want:    5,
			wantErr: false,
		},
		{
			name:    "[1 2 3 4 5] array, move to 6",
			list:    NewDoubleList[int](),
			items:   []int{1, 2, 3, 4, 5},
			moveto:  6,
			want:    1,
			wantErr: true,
		},
		{
			name:    "[1 2 3 4 5] array, move to -1",
			list:    NewDoubleList[int](),
			items:   []int{1, 2, 3, 4, 5},
			moveto:  -1,
			want:    1,
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				tt.list.AddTail(item)
			}

			if err := tt.list.MoveTo(tt.moveto); (err != nil) != tt.wantErr {
				t.Errorf("DoubleList.MoveTo() err := %v, want %v", err, tt.wantErr)
			}

			if got, _ := tt.list.GetCurr(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DoubleList.MoveTo() = %d, want %d", got, tt.want)
			}
		})
	}
}

func TestDoubleList_MoveToHead(t *testing.T) {
	tests := []struct {
		name  string
		list  DoubleLister[int]
		items []int
		want  int
	}{
		{
			name:  "empty list",
			list:  NewDoubleList[int](),
			items: nil,
			want:  0,
		},
		{
			name:  "[1 2 2] array",
			list:  NewDoubleList[int](),
			items: []int{1, 2, 2},
			want:  1,
		},
		{
			name:  "[1] array",
			list:  NewDoubleList[int](),
			items: []int{1},
			want:  1,
		},
		{
			name:  "[1 2 3] array",
			list:  NewDoubleList[int](),
			items: []int{1, 2, 3},
			want:  1,
		},
		{
			name:  "[1 2 3 4 5] array",
			list:  NewDoubleList[int](),
			items: []int{1, 2, 3, 4, 5},
			want:  1,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				tt.list.AddTail(item)
			}

			tt.list.MoveToHead()
			if got, _ := tt.list.GetCurr(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DoubleList.MoveToTail() = %d, want %d", got, tt.want)
			}
		})
	}
}

func TestDoubleList_MoveToTail(t *testing.T) {
	tests := []struct {
		name  string
		list  DoubleLister[int]
		items []int
		want  int
	}{
		{
			name:  "empty list",
			list:  NewDoubleList[int](),
			items: nil,
			want:  0,
		},
		{
			name:  "[2 2 1] array",
			list:  NewDoubleList[int](),
			items: []int{2, 2, 1},
			want:  2,
		},
		{
			name:  "[1] array",
			list:  NewDoubleList[int](),
			items: []int{1},
			want:  1,
		},
		{
			name:  "[1 2 3] array",
			list:  NewDoubleList[int](),
			items: []int{1, 2, 3},
			want:  1,
		},
		{
			name:  "[1 2 3 4 5] array",
			list:  NewDoubleList[int](),
			items: []int{5, 4, 3, 2, 1},
			want:  5,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				tt.list.AddTail(item)
			}

			tt.list.MoveToHead()
			if got, _ := tt.list.GetCurr(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DoubleList.MoveToTail() = %d, want %d", got, tt.want)
			}
		})
	}
}

func TestDoubleList_AddHead_GetHead(t *testing.T) {
	tests := []struct {
		name      string
		list      DoubleLister[int]
		items     []int
		wantCount int
		want      int
	}{
		{
			name:      "add 1 element",
			list:      NewDoubleList[int](),
			items:     []int{1},
			wantCount: 1,
			want:      1,
		},
		{
			name:      "add 3 element",
			list:      NewDoubleList[int](),
			items:     []int{1, 2, 3},
			wantCount: 3,
			want:      3,
		},
		{
			name:      "add 5 element",
			list:      NewDoubleList[int](),
			items:     []int{5, 2, 1, 3, 4},
			wantCount: 5,
			want:      4,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				tt.list.AddHead(item)
			}

			if got := tt.list.Len(); !reflect.DeepEqual(got, tt.wantCount) {
				t.Errorf("DoubleList.AddHead() count = %d, want %d", got, tt.wantCount)
			}

			if got, _ := tt.list.GetHead(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DoubleList.GetHead() item = %d, want %d", got, tt.want)
			}
		})
	}
}

func TestDoubleList_AddCurr(t *testing.T) {
	tests := []struct {
		name     string
		list     DoubleLister[int]
		items    []int
		shift    int
		want     int
		wantList []int
	}{
		{
			name:     "empty list",
			list:     NewDoubleList[int](),
			items:    nil,
			shift:    0,
			want:     5,
			wantList: []int{5},
		},
		{
			name:     "[1 2 3] array, add after 0",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3},
			shift:    0,
			want:     5,
			wantList: []int{1, 5, 2, 3},
		},
		{
			name:     "[1 2 3] array, add after 1",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3},
			shift:    1,
			want:     5,
			wantList: []int{1, 2, 5, 3},
		},
		{
			name:     "[1 2 3] array, add to end",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3},
			shift:    2,
			want:     5,
			wantList: []int{1, 2, 3, 5},
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				tt.list.AddTail(item)
			}

			for i := 0; i != tt.shift; i++ {
				tt.list.Next()
			}

			tt.list.AddCurr(tt.want)
			if got, _ := tt.list.GetCurr(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DoubleList.Prev() = %d, want %d", got, tt.want)
			}

			lst1, _ := tt.list.GetArray()
			lst2, _ := tt.list.GetArrayReverse()
			for i, v := range lst1 {
				if !reflect.DeepEqual(v, tt.wantList[i]) || !reflect.DeepEqual(v, lst2[tt.list.Len()-i-1]) {
					t.Errorf("DoubleList.AddCurr() item %d, want %d", v, tt.wantList[i])
				}
			}
		})
	}
}

func TestDoubleList_AddToIdx(t *testing.T) {
	tests := []struct {
		name     string
		list     DoubleLister[int]
		items    []int
		index    int
		data     int
		wantList []int
		wantErr  bool
	}{
		{
			name:     "empty list",
			list:     NewDoubleList[int](),
			items:    nil,
			index:    0,
			data:     5,
			wantList: []int{5},
			wantErr:  false,
		},
		{
			name:     "[1 2 3] array, add to 0",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3},
			index:    0,
			data:     5,
			wantList: []int{5, 1, 2, 3},
			wantErr:  false,
		},
		{
			name:     "[1 2 3] array, add to 1",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3},
			index:    1,
			data:     5,
			wantList: []int{1, 5, 2, 3},
			wantErr:  false,
		},
		{
			name:     "[1 2 3] array, add to 2",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3},
			index:    2,
			data:     5,
			wantList: []int{1, 2, 5, 3},
			wantErr:  false,
		},
		{
			name:     "[1 2 3] array, add to 3",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3},
			index:    3,
			data:     5,
			wantList: []int{1, 2, 3, 5},
			wantErr:  false,
		},
		{
			name:     "[1 2 3 4] array, add to 1",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3, 4},
			index:    1,
			data:     5,
			wantList: []int{1, 5, 2, 3, 4},
			wantErr:  false,
		},
		{
			name:     "[1 2 3 4] array, add to 2",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3, 4},
			index:    2,
			data:     5,
			wantList: []int{1, 2, 5, 3, 4},
			wantErr:  false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				tt.list.AddTail(item)
			}

			if err := tt.list.AddToIdx(tt.index, tt.data); (err != nil) != tt.wantErr {
				t.Errorf("DoubleList.AddToIdx() err = %v, want %v", err, tt.wantErr)
			}

			lst, _ := tt.list.GetArray()
			for i, v := range lst {
				if !reflect.DeepEqual(v, tt.wantList[i]) {
					t.Errorf("DoubleList.AddCurr() item %d, want %d", v, tt.wantList[i])
				}
			}
		})
	}
}

func TestDoubleList_AddTail_GetTail(t *testing.T) {
	tests := []struct {
		name      string
		list      DoubleLister[int]
		items     []int
		wantCount int
		want      int
	}{
		{
			name:      "add 1 element",
			list:      NewDoubleList[int](),
			items:     []int{1},
			wantCount: 1,
			want:      1,
		},
		{
			name:      "add 3 element",
			list:      NewDoubleList[int](),
			items:     []int{1, 2, 3},
			wantCount: 3,
			want:      3,
		},
		{
			name:      "add 5 element",
			list:      NewDoubleList[int](),
			items:     []int{5, 2, 1, 3, 4},
			wantCount: 5,
			want:      4,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				tt.list.AddTail(item)
			}

			if got := tt.list.Len(); !reflect.DeepEqual(got, tt.wantCount) {
				t.Errorf("DoubleList.AddHead() count = %d, want %d", got, tt.wantCount)
			}
			if got, _ := tt.list.GetTail(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DoubleList.GetTail() item = %d, want %d", got, tt.want)
			}
		})
	}
}

func TestDoubleList_Remove(t *testing.T) {
	tests := []struct {
		name     string
		list     DoubleLister[int]
		items    []int
		data     int
		wantErr  bool
		wantList []int
	}{
		{
			name:     "remove from nil list",
			list:     NewDoubleList[int](),
			items:    nil,
			data:     5,
			wantList: nil,
			wantErr:  true,
		},
		{
			name:     "remove from empty list",
			list:     NewDoubleList[int](),
			items:    []int{},
			data:     1,
			wantList: []int{},
			wantErr:  true,
		},
		{
			name:     "[1] array",
			list:     NewDoubleList[int](),
			items:    []int{1},
			data:     1,
			wantList: []int{},
			wantErr:  false,
		},
		{
			name:     "[1 2 3 4 5] array, data 1",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3, 4, 5},
			data:     1,
			wantList: []int{2, 3, 4, 5},
			wantErr:  false,
		},
		{
			name:     "[1 2 3 4 5] array, data 2",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3, 4, 5},
			data:     2,
			wantList: []int{1, 3, 4, 5},
			wantErr:  false,
		},
		{
			name:     "[1 2 3 4 5] array, data 3",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3, 4, 5},
			data:     3,
			wantList: []int{1, 2, 4, 5},
			wantErr:  false,
		},
		{
			name:     "[1 2 3 4 5] array, data 4",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3, 4, 5},
			data:     4,
			wantList: []int{1, 2, 3, 5},
			wantErr:  false,
		},
		{
			name:     "[1 2 3 4 5] array, data 5",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3, 4, 5},
			data:     5,
			wantList: []int{1, 2, 3, 4},
			wantErr:  false,
		},
		{
			name:     "[1 2 3 4] array, data 2",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3, 4},
			data:     2,
			wantList: []int{1, 3, 4},
			wantErr:  false,
		},
		{
			name:     "[1 2 3 4] array, data 3",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3, 4},
			data:     3,
			wantList: []int{1, 2, 4},
			wantErr:  false,
		},
		{
			name:     "[1 2 3 4] array, data 5",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3, 4},
			data:     5,
			wantList: []int{1, 2, 3, 4},
			wantErr:  true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				tt.list.AddTail(item)
			}

			if err := tt.list.Remove(tt.data); (err != nil) != tt.wantErr {
				t.Errorf("DoubleList.RemoveCurr() err %v, want %v", err, tt.wantErr)
			}

			lst1, _ := tt.list.GetArray()
			lst2, _ := tt.list.GetArrayReverse()
			for i, v := range tt.wantList {
				if !reflect.DeepEqual(lst1[i], v) || !reflect.DeepEqual(lst2[tt.list.Len()-i-1], v) {
					t.Errorf("DoubleList.RemoveCurr() item %d, want %d", v, tt.wantList[i])
				}
			}
		})
	}

}

func TestDoubleList_RemoveByIdx(t *testing.T) {
	tests := []struct {
		name     string
		list     DoubleLister[int]
		items    []int
		index    int
		wantErr  bool
		wantList []int
	}{
		{
			name:     "remove from nil list",
			list:     NewDoubleList[int](),
			items:    nil,
			index:    5,
			wantList: nil,
			wantErr:  true,
		},
		{
			name:     "remove from empty list",
			list:     NewDoubleList[int](),
			items:    []int{},
			index:    0,
			wantList: []int{},
			wantErr:  true,
		},
		{
			name:     "[1] array",
			list:     NewDoubleList[int](),
			items:    []int{1},
			index:    0,
			wantList: []int{},
			wantErr:  false,
		},
		{
			name:     "[1 2 3 4 5] array, index 0",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3, 4, 5},
			index:    0,
			wantList: []int{2, 3, 4, 5},
			wantErr:  false,
		},
		{
			name:     "[1 2 3 4 5] array, index 1",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3, 4, 5},
			index:    1,
			wantList: []int{1, 3, 4, 5},
			wantErr:  false,
		},
		{
			name:     "[1 2 3 4 5] array, index 2",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3, 4, 5},
			index:    2,
			wantList: []int{1, 2, 4, 5},
			wantErr:  false,
		},
		{
			name:     "[1 2 3 4 5] array, index 3",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3, 4, 5},
			index:    3,
			wantList: []int{1, 2, 3, 5},
			wantErr:  false,
		},
		{
			name:     "[1 2 3 4 5] array, index 4",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3, 4, 5},
			index:    4,
			wantList: []int{1, 2, 3, 4},
			wantErr:  false,
		},
		{
			name:     "[1 2 3 4] array, index 1",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3, 4},
			index:    1,
			wantList: []int{1, 3, 4},
			wantErr:  false,
		},
		{
			name:     "[1 2 3 4] array, index 2",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3, 4},
			index:    2,
			wantList: []int{1, 2, 4},
			wantErr:  false,
		},
		{
			name:     "[1 2 3 4] array, index 5",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3, 4},
			index:    5,
			wantList: []int{1, 2, 3, 4},
			wantErr:  true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				tt.list.AddTail(item)
			}

			if err := tt.list.RemoveByIdx(tt.index); (err != nil) != tt.wantErr {
				t.Errorf("DoubleList.RemoveCurr() err %v, want %v", err, tt.wantErr)
			}

			lst1, _ := tt.list.GetArray()
			lst2, _ := tt.list.GetArrayReverse()
			for i, v := range tt.wantList {
				if !reflect.DeepEqual(lst1[i], v) || !reflect.DeepEqual(lst2[tt.list.Len()-i-1], v) {
					t.Errorf("DoubleList.RemoveCurr() item %d, want %d", v, tt.wantList[i])
				}
			}
		})
	}
}

func TestDoubleList_RemoveHead(t *testing.T) {
	tests := []struct {
		name     string
		list     DoubleLister[int]
		items    []int
		wantErr  bool
		wantList []int
	}{
		{
			name:     "remove from nil list",
			list:     NewDoubleList[int](),
			items:    nil,
			wantList: nil,
			wantErr:  true,
		},
		{
			name:     "remove from empty list",
			list:     NewDoubleList[int](),
			items:    []int{},
			wantList: []int{},
			wantErr:  true,
		},
		{
			name:     "[1 2 3] array, remove tail",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3},
			wantList: []int{2, 3},
			wantErr:  false,
		},
		{
			name:     "[1 2] array, remove tail",
			list:     NewDoubleList[int](),
			items:    []int{2, 3},
			wantList: []int{3},
			wantErr:  false,
		},
		{
			name:     "[1] array, remove tail",
			list:     NewDoubleList[int](),
			items:    []int{3},
			wantList: []int{},
			wantErr:  false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				tt.list.AddTail(item)
			}

			if err := tt.list.RemoveHead(); (err != nil) != tt.wantErr {
				t.Errorf("DoubleList.RemoveHead() err %v, want %v", err, tt.wantErr)
			}

			lst1, _ := tt.list.GetArray()
			lst2, _ := tt.list.GetArrayReverse()
			for i, v := range tt.wantList {
				if !reflect.DeepEqual(lst1[i], v) || !reflect.DeepEqual(lst2[tt.list.Len()-i-1], v) {
					t.Errorf("DoubleList.RemoveHead() item %d, want %d", v, tt.wantList[i])
				}
			}
		})
	}
}

func TestDoubleList_RemoveCurr(t *testing.T) {
	tests := []struct {
		name     string
		list     DoubleLister[int]
		items    []int
		shift    int
		wantErr  bool
		wantList []int
	}{
		{
			name:     "remove from nil list",
			list:     NewDoubleList[int](),
			items:    nil,
			wantList: nil,
			wantErr:  true,
		},
		{
			name:     "remove from empty list",
			list:     NewDoubleList[int](),
			items:    []int{},
			wantList: []int{},
			wantErr:  true,
		},
		{
			name:     "[1] array",
			list:     NewDoubleList[int](),
			items:    []int{1},
			wantList: []int{},
			wantErr:  false,
		},
		{
			name:     "[1 2 3 4 5] array, shift 0",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3, 4, 5},
			shift:    0,
			wantList: []int{2, 3, 4, 5},
			wantErr:  false,
		},
		{
			name:     "[1 2 3 4 5] array, shift 1",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3, 4, 5},
			shift:    1,
			wantList: []int{1, 3, 4, 5},
			wantErr:  false,
		},
		{
			name:     "[1 2 3 4 5] array, shift 2",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3, 4, 5},
			shift:    2,
			wantList: []int{1, 2, 4, 5},
			wantErr:  false,
		},
		{
			name:     "[1 2 3 4 5] array, shift 3",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3, 4, 5},
			shift:    3,
			wantList: []int{1, 2, 3, 5},
			wantErr:  false,
		},
		{
			name:     "[1 2 3 4 5] array, shift 4",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3, 4, 5},
			shift:    4,
			wantList: []int{1, 2, 3, 4},
			wantErr:  false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				tt.list.AddTail(item)
			}

			for i := 0; i != tt.shift; i++ {
				tt.list.Next()
			}

			if err := tt.list.RemoveCurr(); (err != nil) != tt.wantErr {
				t.Errorf("DoubleList.RemoveCurr() err %v, want %v", err, tt.wantErr)
			}

			lst1, _ := tt.list.GetArray()
			lst2, _ := tt.list.GetArrayReverse()
			for i, v := range tt.wantList {
				if !reflect.DeepEqual(lst1[i], v) || !reflect.DeepEqual(lst2[tt.list.Len()-i-1], v) {
					t.Errorf("DoubleList.RemoveCurr() item %d, want %d", v, tt.wantList[i])
				}
			}
		})
	}

}

func TestDoubleList_RemoveTail(t *testing.T) {
	tests := []struct {
		name     string
		list     DoubleLister[int]
		items    []int
		wantErr  bool
		wantList []int
	}{
		{
			name:     "remove from nil list",
			list:     NewDoubleList[int](),
			items:    nil,
			wantList: nil,
			wantErr:  true,
		},
		{
			name:     "remove from empty list",
			list:     NewDoubleList[int](),
			items:    []int{},
			wantList: []int{},
			wantErr:  true,
		},
		{
			name:     "[1 2 3] array, remove tail",
			list:     NewDoubleList[int](),
			items:    []int{1, 2, 3},
			wantList: []int{1, 2},
			wantErr:  false,
		},
		{
			name:     "[1 2] array, remove tail",
			list:     NewDoubleList[int](),
			items:    []int{1, 2},
			wantList: []int{1},
			wantErr:  false,
		},
		{
			name:     "[1] array, remove tail",
			list:     NewDoubleList[int](),
			items:    []int{1},
			wantList: []int{},
			wantErr:  false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				tt.list.AddTail(item)
			}

			if err := tt.list.RemoveTail(); (err != nil) != tt.wantErr {
				t.Errorf("DoubleList.RemoveTail() err %v, want %v", err, tt.wantErr)
			}

			lst1, _ := tt.list.GetArray()
			lst2, _ := tt.list.GetArrayReverse()
			for i, v := range tt.wantList {
				if !reflect.DeepEqual(lst1[i], v) || !reflect.DeepEqual(lst2[tt.list.Len()-i-1], v) {
					t.Errorf("DoubleList.RemoveTail() item %d, want %d", v, tt.wantList[i])
				}
			}
		})
	}
}

func TestDoubleList_GetArray(t *testing.T) {
	tests := []struct {
		name    string
		list    DoubleLister[int]
		items   []int
		want    []int
		wantErr bool
	}{
		{
			name:    "1 element",
			list:    NewDoubleList[int](),
			items:   []int{1},
			want:    []int{1},
			wantErr: false,
		},
		{
			name:    "5 element",
			list:    NewDoubleList[int](),
			items:   []int{1, 2, 3, 4, 5},
			want:    []int{1, 2, 3, 4, 5},
			wantErr: false,
		},
		{
			name:    "0 element",
			list:    NewDoubleList[int](),
			items:   []int{},
			want:    nil,
			wantErr: true,
		},
		{
			name:    "10 element",
			list:    NewDoubleList[int](),
			items:   []int{10, 6, 8, 4, 1150, 100500, 14, 10, 2, 99},
			want:    []int{10, 6, 8, 4, 1150, 100500, 14, 10, 2, 99},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				tt.list.AddTail(item)
			}

			got, err := tt.list.GetArray()

			if (err != nil) != tt.wantErr {
				t.Errorf("DoubleList.GetArray() err = %v, want %v", err, tt.wantErr)
			}

			for i := range tt.want {
				if !reflect.DeepEqual(got[i], tt.want[i]) {
					t.Errorf("DobuleList.GetArray() item by index %d = %d, want %d", i, got[i], tt.want[i])
				}
			}
		})
	}
}

func TestDoubleList_GetArrayReverse(t *testing.T) {
	tests := []struct {
		name    string
		list    DoubleLister[int]
		items   []int
		want    []int
		wantErr bool
	}{
		{
			name:    "1 element",
			list:    NewDoubleList[int](),
			items:   []int{1},
			want:    []int{1},
			wantErr: false,
		},
		{
			name:    "5 element",
			list:    NewDoubleList[int](),
			items:   []int{1, 2, 3, 4, 5},
			want:    []int{5, 4, 3, 2, 1},
			wantErr: false,
		},
		{
			name:    "0 element",
			list:    NewDoubleList[int](),
			items:   []int{},
			want:    nil,
			wantErr: true,
		},
		{
			name:    "10 element",
			list:    NewDoubleList[int](),
			items:   []int{10, 6, 8, 4, 1150, 100500, 14, 10, 2, 99},
			want:    []int{99, 2, 10, 14, 100500, 1150, 4, 8, 6, 10},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				tt.list.AddTail(item)
			}

			got, err := tt.list.GetArrayReverse()

			if (err != nil) != tt.wantErr {
				t.Errorf("DoubleList.GetArray() err = %v, want %v", err, tt.wantErr)
			}

			for i := range tt.want {
				if !reflect.DeepEqual(got[i], tt.want[i]) {
					t.Errorf("DobuleList.GetArray() item by index %d = %d, want %d", i, got[i], tt.want[i])
				}
			}
		})
	}
}

func TestDoubleList_Contains(t *testing.T) {
	tests := []struct {
		name   string
		list   DoubleLister[int]
		items  []int
		search int
		want   bool
	}{
		{
			name:   "[1] array, search 1",
			list:   NewDoubleList[int](),
			items:  []int{1},
			search: 1,
			want:   true,
		},
		{
			name:   "[1 2 3 4 5] array, search 3",
			list:   NewDoubleList[int](),
			items:  []int{1, 2, 3, 4, 5},
			search: 3,
			want:   true,
		},
		{
			name:   "[1 2 3 4 5] array, search 1",
			list:   NewDoubleList[int](),
			items:  []int{1, 2, 3, 4, 5},
			search: 1,
			want:   true,
		},
		{
			name:   "[1 2 3 4 5] array, search 5",
			list:   NewDoubleList[int](),
			items:  []int{1, 2, 3, 4, 5},
			search: 5,
			want:   true,
		},
		{
			name:   "[1 2 3 4 5] array, search 2",
			list:   NewDoubleList[int](),
			items:  []int{1, 2, 3, 4, 5},
			search: 2,
			want:   true,
		},
		{
			name:   "[1 2 3 4 5] array, search 4",
			list:   NewDoubleList[int](),
			items:  []int{1, 2, 3, 4, 5},
			search: 4,
			want:   true,
		},
		{
			name:   "[1 2 3 4 5 6] array, search 4",
			list:   NewDoubleList[int](),
			items:  []int{1, 2, 3, 4, 5, 6},
			search: 4,
			want:   true,
		},
		{
			name:   "[1 2 3 4 5] array, search 10",
			list:   NewDoubleList[int](),
			items:  []int{1, 2, 3, 4, 5},
			search: 10,
			want:   false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				tt.list.AddTail(item)
			}

			if got := tt.list.Contains(tt.search); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DobuleList.Contains() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDoubleList_Len(t *testing.T) {
	tests := []struct {
		name  string
		list  DoubleLister[int]
		items []int
		want  int
	}{
		{
			name:  "1 element",
			list:  NewDoubleList[int](),
			items: []int{1},
			want:  1,
		},
		{
			name:  "5 element",
			list:  NewDoubleList[int](),
			items: []int{1, 2, 3, 4, 5},
			want:  5,
		},
		{
			name:  "0 element",
			list:  NewDoubleList[int](),
			items: []int{},
			want:  0,
		},
		{
			name:  "10 element",
			list:  NewDoubleList[int](),
			items: []int{10, 6, 8, 4, 1150, 100500, 14, 10, 2, 99},
			want:  10,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				tt.list.AddTail(item)
			}

			if got := tt.list.Len(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("DobuleList.Len() = %d, want %d", got, tt.want)
			}
		})
	}
}
