package linkedlist

import "errors"

type SingleList[T comparable] struct {
	head *singleListNode[T]
	tail *singleListNode[T]
	len  int
}

func NewSingleList[T comparable]() SingleLister[T] {
	return &SingleList[T]{
		head: nil,
		len:  0,
	}
}

func (sl *SingleList[T]) Len() int {
	return sl.len
}

// Contains method search data in linked list and return true if contains else false
// Difficulty: O(N)
func (sl *SingleList[T]) Contains(data T) bool {
	if sl.len == 0 {
		return false
	}
	if sl.head.data == data || sl.tail.data == data {
		return true
	}

	curr := sl.head.next
	for curr != nil {
		if curr.data == data {
			return true
		}
		curr = curr.next
	}

	return false
}

// Add method add data to the end of linked list. If list is empty create head and tail elements
// Difficulty: O(1)
func (sl *SingleList[T]) Add(data T) {
	node := &singleListNode[T]{
		data: data,
	}

	if sl.len == 0 {
		sl.head = node
		sl.tail = node
	} else {
		sl.tail.next = node
		sl.tail = node
	}

	sl.len++
}

// Remove method remove target element from linked list. If LL doesn't contain element return error
// Difficulty: O(N)
func (sl *SingleList[T]) Remove(data T) error {
	if sl.len == 0 {
		return errors.New(emptyList)
	}

	var prev *singleListNode[T]
	curr := sl.head
	for curr != nil {
		if curr.data == data {
			if prev != nil {
				// before: head -> 1 -> 2 -> 3 -> nil
				// after:  head -> 1 -> 2 ------> nil
				prev.next = curr.next

				if curr.next == nil {
					sl.tail = prev
				}
			} else {
				// before: head -> 3 -> 5 -> nil
				// after:  head ------> 5 -> nil
				sl.head = curr.next

				// before: head -> 3 -> nil
				// after:  head ------> nil
				if sl.head == nil {
					sl.tail = nil
				}
			}

			sl.len--
			return nil
		}

		prev = curr
		curr = curr.next
	}

	return errors.New(notFound)
}

// Shift method remove and return element from LL head
// Difficulty: O(1)
func (sl *SingleList[T]) Shift() (T, error) {
	var res T
	if sl.len == 0 {
		return res, errors.New(emptyList)
	}

	res = sl.head.data

	if sl.head.next != nil {
		sl.head = sl.head.next
	} else {
		sl.head = nil
		sl.tail = nil
	}

	sl.len--

	return res, nil
}

// Pop method remove and return element from LL tail
// Difficulty: O(N)
func (sl *SingleList[T]) Pop() (T, error) {
	var res T
	if sl.len == 0 {
		return res, errors.New(emptyList)
	}

	res = sl.tail.data

	if sl.head.next == nil {
		sl.head = nil
		sl.tail = nil
		sl.len--
		return res, nil
	}

	curr := sl.head
	for curr.next != nil {
		if curr.next.next == nil {
			curr.next = nil
		} else {
			curr = curr.next
		}
	}

	sl.len--
	sl.tail = curr
	return res, nil
}
