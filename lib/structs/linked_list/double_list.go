package linkedlist

import (
	"errors"
)

type DoubleList[T comparable] struct {
	head *doubleListNode[T]
	curr *doubleListNode[T]
	tail *doubleListNode[T]
	len  int
}

func NewDoubleList[T comparable]() DoubleLister[T] {
	return &DoubleList[T]{
		head: nil,
		curr: nil,
		tail: nil,
		len:  0,
	}
}

// Next method move current node to next node if it exists
// Difficulty: O(1)
func (dl *DoubleList[T]) Next() {
	if dl.curr != nil && dl.curr.next != nil {
		dl.curr = dl.curr.next
	}
}

// GetCurr method return current data from current node.
func (dl *DoubleList[T]) GetCurr() (T, error) {
	var out T
	if dl.len == 0 {
		return out, errors.New(emptyList)
	}
	return dl.curr.data, nil
}

// GetHead method return data from head node.
func (dl *DoubleList[T]) GetHead() (T, error) {
	var out T
	if dl.len == 0 {
		return out, errors.New(emptyList)
	}
	return dl.head.data, nil
}

// GetTail method return data from tail node.
func (dl *DoubleList[T]) GetTail() (T, error) {
	var out T
	if dl.len == 0 {
		return out, errors.New(emptyList)
	}
	return dl.tail.data, nil
}

// Prev method move current node to previous node if it exists
// Difficulty: O(1)
func (dl *DoubleList[T]) Prev() {
	if dl.curr != nil && dl.curr.prev != nil {
		dl.curr = dl.curr.prev
	}
}

// MoveToHead method move current position to tail node
// Difficulty: O(1)
func (dl *DoubleList[T]) MoveToHead() {
	if dl.head != nil {
		dl.curr = dl.head
	}
}

// MoveToTail method move current position to tail node
// Difficulty: O(1)
func (dl *DoubleList[T]) MoveToTail() {
	if dl.head != nil {
		dl.curr = dl.tail
	}
}

// MoveTo method move current node to node by index. Return error if index is out of range
// Difficulty: O(n)
func (dl *DoubleList[T]) MoveTo(idx int) error {
	if idx > dl.len-1 || idx < 0 {
		return errors.New(outOfRange)
	}

	var pointer int
	var curr *doubleListNode[T]
	if idx < dl.len/2 {
		pointer = 0
		curr = dl.head
		for pointer <= dl.len/2 {
			if pointer == idx {
				dl.curr = curr
				return nil
			}
			curr = curr.next
			pointer++
		}
	} else {
		pointer = dl.len - 1
		curr = dl.tail
		for pointer >= dl.len/2 {
			if pointer == idx {
				dl.curr = curr
				return nil
			}
			curr = curr.prev
			pointer--
		}
	}

	return errors.New(notFound)
}

// AddHead method add element to head. If Linked List is empty, data move to head & tail
// Difficulty: O(1)
func (dl *DoubleList[T]) AddHead(data T) {
	node := &doubleListNode[T]{data: data}
	if dl.len == 0 {
		dl.head = node
		dl.tail = node
		dl.curr = node
	} else {
		dl.head.prev = node
		node.next = dl.head
		dl.head = node
	}

	dl.len++
}

// AddCurr method add new node after current and make it current
// Difficulty: O(1)
func (dl *DoubleList[T]) AddCurr(data T) {
	node := &doubleListNode[T]{data: data}
	if dl.len == 0 {
		dl.head = node
		dl.tail = node
		dl.curr = node
	} else if dl.curr == dl.tail {
		dl.tail.next = node
		node.prev = dl.tail
		dl.tail = node

		dl.curr = dl.tail
	} else {
		node.prev, node.next = dl.curr, dl.curr.next
		dl.curr.next.prev = node
		dl.curr.next = node
		dl.curr = node
	}
	dl.len++
}

// AddToIdx method add new node to index and move forward current index, if it exists
// Difficulty: O(n)
func (dl *DoubleList[T]) AddToIdx(idx int, data T) error {
	switch idx {
	case 0:
		dl.AddHead(data)
		return nil
	case dl.len:
		dl.AddTail(data)
		return nil
	}
	if idx > dl.len-1 || idx < 0 {
		return errors.New(outOfRange)
	}

	var pointer int
	var curr *doubleListNode[T]
	node := &doubleListNode[T]{data: data}
	if idx < dl.len/2 {
		pointer = 0
		curr = dl.head
		for pointer <= dl.len/2 {
			if pointer == idx {
				node.prev, node.next = curr.prev, curr
				curr.prev.next = node
				curr.prev = node
				curr = node
				return nil
			}
			curr = curr.next
			pointer++
		}
	} else {
		pointer = dl.len - 1
		curr = dl.tail
		for pointer >= dl.len/2 {
			if pointer == idx {
				node.prev, node.next = curr.prev, curr
				curr.prev.next = node
				curr.prev = node
				curr = node
				return nil
			}
			curr = curr.prev
			pointer--
		}
	}

	return errors.New(notFound)
}

// AddTail method add element to tail. If Linked List is empty, data move to head & tail
// Difficulty: O(1)
func (dl *DoubleList[T]) AddTail(data T) {
	node := &doubleListNode[T]{data: data}
	if dl.len == 0 {
		dl.head = node
		dl.tail = node
		dl.curr = node
	} else {
		dl.tail.next = node
		node.prev = dl.tail
		dl.tail = node
	}

	dl.len++
}

// Remove method  remove data from linked list.
// Difficulty: O(n/2)
func (dl *DoubleList[T]) Remove(data T) error {
	if dl.len == 0 {
		return errors.New(emptyList)
	} else if dl.len == 1 && dl.head.data == data {
		dl.cleanList()
		return nil
	} else if dl.head.data == data {
		return dl.RemoveHead()
	} else if dl.tail.data == data {
		return dl.RemoveTail()
	}

	left, right := dl.head, dl.tail
	for left.prev != right {
		if left.data == data {
			left.prev.next, left.next.prev = left.next, left.prev
			left = nil
			dl.len--
			return nil
		}
		if right.data == data {
			right.prev.next, right.next.prev = right.next, right.prev
			right = nil
			dl.len--
			return nil
		}

		left = left.next
		right = right.prev
	}

	return errors.New(notFound)
}

// RemoveByIdx method remove data from linked list by id.
// Diffiulty: O(n)
func (dl *DoubleList[T]) RemoveByIdx(idx int) error {
	if dl.len == 0 {
		return errors.New(emptyList)
	} else if idx > dl.len-1 || idx < 0 {
		return errors.New(outOfRange)
	} else if dl.len == 1 && idx == 1 {
		dl.cleanList()
		return nil
	} else if idx == 0 {
		return dl.RemoveHead()
	} else if idx == dl.len-1 {
		return dl.RemoveTail()
	}

	var pointer int
	var curr *doubleListNode[T]
	if idx < dl.len/2 {
		pointer = 0
		curr = dl.head
		for pointer <= dl.len/2 {
			if pointer == idx {
				curr.prev.next, curr.next.prev = curr.next, curr.prev
				curr = nil
				dl.len--
				return nil
			}
			curr = curr.next
			pointer++
		}
	} else {
		pointer = dl.len - 1
		curr = dl.tail
		for pointer >= dl.len/2 {
			if pointer == idx {
				curr.prev.next, curr.next.prev = curr.next, curr.prev
				curr = nil
				dl.len--
				return nil
			}
			curr = curr.prev
			pointer--
		}
	}

	return errors.New(notFound)

}

// RemoveHead method remove first element from linked list. Return error if linked list is empty
// Difficulty: O(1)
func (dl *DoubleList[T]) RemoveHead() error {
	if dl.len == 0 {
		return errors.New(emptyList)
	} else if dl.len == 1 {
		dl.cleanList()
		return nil
	}

	if dl.curr == dl.head {
		dl.curr = dl.head.next
		dl.curr.prev = nil
	}

	dl.head = dl.head.next
	dl.head.prev = nil
	dl.len--

	return nil
}

// RemoveCurr method remove current element from linked list. Return error if linked list is empty.
// Difficulty: O(1)
func (dl *DoubleList[T]) RemoveCurr() error {
	if dl.len == 0 {
		return errors.New(emptyList)
	} else if dl.len == 1 {
		dl.cleanList()
		return nil
	}

	if dl.curr == dl.tail {
		return dl.RemoveTail()
	} else if dl.curr == dl.head {
		return dl.RemoveHead()
	}

	curr := dl.curr
	curr.prev.next, curr.next.prev = curr.next, curr.prev
	dl.curr = dl.curr.next
	curr = nil

	dl.len--
	return nil
}

// RemoveTail method remove tail node from linked list. Return error if linked list is empty.
// Difficulty: O(1)
func (dl *DoubleList[T]) RemoveTail() error {
	if dl.len == 0 {
		return errors.New(emptyList)
	} else if dl.len == 1 {
		dl.cleanList()
		return nil
	}

	if dl.curr == dl.tail {
		dl.curr = dl.tail.prev
		dl.curr.next = nil
	}

	dl.tail = dl.tail.prev
	dl.tail.next = nil
	dl.len--

	return nil
}

// GetArray method return array of linked list
// Difficulty: O(n)
func (dl *DoubleList[T]) GetArray() ([]T, error) {
	if dl.len == 0 {
		return nil, errors.New(emptyList)
	}

	out := make([]T, 0, dl.len)
	curr := dl.head
	for curr != nil {
		out = append(out, curr.data)
		curr = curr.next
	}

	return out, nil
}

// GetArrayReverse method return array of reversed linked list
// Difficulty: O(n)
func (dl *DoubleList[T]) GetArrayReverse() ([]T, error) {
	if dl.len == 0 {
		return nil, errors.New(emptyList)
	}

	out := make([]T, 0, dl.len)
	curr := dl.tail
	for curr != nil {
		out = append(out, curr.data)
		curr = curr.prev
	}

	return out, nil
}

// Contains method check if there is data in the linked list
// Difficulty: O(n/2)
func (dl *DoubleList[T]) Contains(data T) bool {
	if dl.len == 0 {
		return false
	}

	if dl.head.data == data || dl.tail.data == data {
		return true
	}

	left, right := dl.head, dl.tail
	for left != right || left.prev == right {
		if left.data == data || right.data == data {
			return true
		}
		left = left.next
		right = right.prev
	}

	return left.data == data
}

func (dl *DoubleList[T]) Len() int {
	return dl.len
}

func (dl *DoubleList[T]) cleanList() {
	dl.head = nil
	dl.curr = nil
	dl.tail = nil
	dl.len--
}
