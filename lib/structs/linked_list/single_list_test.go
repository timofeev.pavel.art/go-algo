package linkedlist

import (
	"reflect"
	"testing"
)

func TestSingleList_Contains(t *testing.T) {
	tests := []struct {
		name   string
		list   SingleLister[int]
		items  []int
		search int
		want   bool
	}{
		{
			name:   "one element",
			list:   NewSingleList[int](),
			items:  []int{1},
			search: 1,
			want:   true,
		},
		{
			name:   "3 element, search middle",
			list:   NewSingleList[int](),
			items:  []int{1, 2, 3},
			search: 2,
			want:   true,
		},
		{
			name:   "3 element, search first",
			list:   NewSingleList[int](),
			items:  []int{1, 2, 3},
			search: 1,
			want:   true,
		},
		{
			name:   "3 element, search last",
			list:   NewSingleList[int](),
			items:  []int{1, 2, 3},
			search: 3,
			want:   true,
		},
		{
			name:   "3 element, doesn't contains",
			list:   NewSingleList[int](),
			items:  []int{1, 2, 3},
			search: 4,
			want:   false,
		},
		{
			name:   "zero elements",
			list:   NewSingleList[int](),
			items:  []int{},
			search: 4,
			want:   false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				tt.list.Add(item)
			}

			if got := tt.list.Contains(tt.search); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SingleList.Contains() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSingleList_Add(t *testing.T) {
	tests := []struct {
		name  string
		list  SingleLister[int]
		items []int
		want  bool
	}{
		{
			name:  "add one element",
			list:  NewSingleList[int](),
			items: []int{1},
			want:  true,
		},
		{
			name:  "three elements",
			list:  NewSingleList[int](),
			items: []int{1, 2, 3},
			want:  true,
		},
		{
			name:  "five elements",
			list:  NewSingleList[int](),
			items: []int{1, 2, 3, 4, 5},
			want:  true,
		},
		{
			name:  "zero elements",
			list:  NewSingleList[int](),
			items: []int{},
			want:  false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				tt.list.Add(item)
			}

			for _, item := range tt.items {
				if got := tt.list.Contains(item); !reflect.DeepEqual(got, tt.want) {
					t.Errorf("SingleList.Add() = %v, want %v", got, tt.want)
				}
			}
		})
	}
}

func TestSingleList_Remove(t *testing.T) {
	tests := []struct {
		name      string
		list      SingleLister[int]
		items     []int
		removable int
		want      []int
		contains  []bool
		wantErr   bool
	}{
		{
			name:      "[1] list, remove 1",
			list:      NewSingleList[int](),
			items:     []int{1},
			removable: 1,
			want:      []int{},
			contains:  []bool{},
			wantErr:   false,
		},
		{
			name:      "[1 2 3] list, remove 2",
			list:      NewSingleList[int](),
			items:     []int{1, 2, 3},
			removable: 2,
			want:      []int{1, 3},
			contains:  []bool{true, true},
			wantErr:   false,
		},
		{
			name:      "[1 2 3] list, remove 3",
			list:      NewSingleList[int](),
			items:     []int{1, 2, 3},
			removable: 3,
			want:      []int{1, 2},
			contains:  []bool{true, true},
			wantErr:   false,
		},
		{
			name:      "[1 2 3] list, remove 1",
			list:      NewSingleList[int](),
			items:     []int{1, 2, 3},
			removable: 1,
			want:      []int{2, 3},
			contains:  []bool{true, true},
			wantErr:   false,
		},
		{
			name:      "[1 2 3] list, remove 4",
			list:      NewSingleList[int](),
			items:     []int{1, 2, 3},
			removable: 4,
			want:      []int{1, 2, 3},
			contains:  []bool{true, true, true},
			wantErr:   true,
		},
		{
			name:      "remove from zero list",
			list:      NewSingleList[int](),
			items:     []int{},
			removable: 4,
			want:      []int{},
			contains:  []bool{},
			wantErr:   true,
		},
		{
			name:      "[1 2 3 4] list, remove 4, but compare with [1 2 3 4]",
			list:      NewSingleList[int](),
			items:     []int{1, 2, 3, 4},
			removable: 4,
			want:      []int{1, 2, 3, 4},
			contains:  []bool{true, true, true, false},
			wantErr:   false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				tt.list.Add(item)
			}

			if err := tt.list.Remove(tt.removable); (err != nil) != tt.wantErr {
				t.Errorf("SingleList.Remove() = %v, want %v", err, tt.wantErr)
			}

			for i, item := range tt.want {
				if got := tt.list.Contains(item); !reflect.DeepEqual(got, tt.contains[i]) {
					t.Errorf("SingleList.Contains() = %v, want %v", got, tt.contains)
				}
			}
		})
	}
}

func TestSingleList_Shift(t *testing.T) {
	tests := []struct {
		name    string
		list    SingleLister[int]
		items   []int
		want    int
		wantErr bool
	}{
		{
			name:    "[1] list",
			list:    NewSingleList[int](),
			items:   []int{1},
			want:    1,
			wantErr: false,
		},
		{
			name:    "[1 2 3] list",
			list:    NewSingleList[int](),
			items:   []int{1, 2, 3},
			want:    1,
			wantErr: false,
		},
		{
			name:    "remove from zero list",
			list:    NewSingleList[int](),
			items:   []int{},
			want:    0,
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				tt.list.Add(item)
			}

			got, err := tt.list.Shift()
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SingleList.Shift() = %v, want %v", got, tt.want)
			}
			if (err != nil) != tt.wantErr {
				t.Errorf("SingleList.Shift() = %v, want %v", err, tt.wantErr)
			}
		})
	}
}

func TestSingleList_Pop(t *testing.T) {
	tests := []struct {
		name    string
		list    SingleLister[int]
		items   []int
		want    int
		wantErr bool
	}{
		{
			name:    "[1] list",
			list:    NewSingleList[int](),
			items:   []int{1},
			want:    1,
			wantErr: false,
		},
		{
			name:    "[1 2 3] list",
			list:    NewSingleList[int](),
			items:   []int{1, 2, 3},
			want:    3,
			wantErr: false,
		},
		{
			name:    "remove from zero list",
			list:    NewSingleList[int](),
			items:   []int{},
			want:    0,
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				tt.list.Add(item)
			}

			got, err := tt.list.Pop()
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SingleList.Pop() = %v, want %v", got, tt.want)
			}
			if (err != nil) != tt.wantErr {
				t.Errorf("SingleList.Pop() = %v, want %v", err, tt.wantErr)
			}
		})
	}
}

func TestSingleList_Len(t *testing.T) {
	tests := []struct {
		name  string
		list  SingleLister[int]
		items []int
		want  int
	}{
		{
			name:  "[1] list",
			list:  NewSingleList[int](),
			items: []int{1},
			want:  1,
		},
		{
			name:  "[1 2 3] list",
			list:  NewSingleList[int](),
			items: []int{1, 2, 3},
			want:  3,
		},
		{
			name:  "remove from zero list",
			list:  NewSingleList[int](),
			items: []int{},
			want:  0,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				tt.list.Add(item)
			}

			if got := tt.list.Len(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SingleList.Pop() = %v, want %v", got, tt.want)
			}
		})
	}
}
