package stack

import (
	"errors"
	"sync"
)

type Stack[T any] struct {
	data     []T
	position int
	mu       sync.Mutex
}

func NewStack[T any](cap int) Stacker[T] {
	return &Stack[T]{
		data:     make([]T, 0, cap),
		position: -1,
		mu:       sync.Mutex{},
	}
}

// Push method push element on top of the stack. If stack is empty, then return error
// Difficulty: O(1)
func (s *Stack[T]) Push(data T) error {
	s.mu.Lock()
	defer s.mu.Unlock()

	if s.IsFull() {
		return errors.New(isFull)
	}

	s.data = append(s.data, data)
	s.position++
	return nil
}

// Pop method remove element from the stack and return. If stack is empty, return error
// Difficulty: O(1)
func (s *Stack[T]) Pop() (T, error) {
	s.mu.Lock()
	defer s.mu.Unlock()

	var out T
	if s.IsEmpty() {
		return out, errors.New(isEmpty)
	}

	out = s.data[s.position]
	s.data = s.data[:s.position]

	s.position--
	return out, nil
}

// Peek method returns the top element of the stack without remove
// Difficulty: O(1)
func (s *Stack[T]) Peek() (T, error) {
	s.mu.Lock()
	defer s.mu.Unlock()

	var out T
	if s.IsEmpty() {
		return out, errors.New(isEmpty)
	}

	return s.data[s.position], nil
}

func (s *Stack[T]) IsEmpty() bool {
	return s.position == -1
}

func (s *Stack[T]) IsFull() bool {
	return s.position == cap(s.data)-1
}

func (s *Stack[T]) Len() int {
	return s.position + 1
}

func (s *Stack[T]) Cap() int {
	return cap(s.data)
}
