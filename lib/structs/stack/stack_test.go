package stack

import (
	"reflect"
	"testing"
)

type testData struct {
	id int
}

func TestStack_Push(t *testing.T) {
	tests := []struct {
		name    string
		stack   Stacker[testData]
		items   []testData
		wantErr bool
	}{
		{
			name:    "add one",
			stack:   NewStack[testData](5),
			items:   []testData{{id: 1}},
			wantErr: false,
		},
		{
			name:    "fill stack",
			stack:   NewStack[testData](5),
			items:   []testData{{id: 1}, {id: 2}, {id: 3}, {id: 4}, {id: 5}},
			wantErr: false,
		},
		{
			name:    "stack overflow",
			stack:   NewStack[testData](5),
			items:   []testData{{id: 1}, {id: 2}, {id: 3}, {id: 4}, {id: 5}, {id: 6}},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			var err error
			for _, item := range tt.items {
				err = tt.stack.Push(item)
			}
			if (err != nil) != tt.wantErr {
				t.Errorf("Stack.Push() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestStack_Pop(t *testing.T) {
	tests := []struct {
		name    string
		stack   Stacker[testData]
		items   []testData
		want    testData
		wantErr bool
	}{
		{
			name:    "pop one",
			stack:   NewStack[testData](5),
			items:   []testData{{id: 1}},
			want:    testData{id: 1},
			wantErr: false,
		},
		{
			name:    "stack is empty",
			stack:   NewStack[testData](5),
			items:   []testData{},
			want:    testData{},
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				_ = tt.stack.Push(item)
			}

			got, err := tt.stack.Pop()
			if (err != nil) != tt.wantErr {
				t.Errorf("Stack.Pop() error = %v, wantErr %v", err, tt.wantErr)
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Stack.Pop() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStack_Peek(t *testing.T) {
	tests := []struct {
		name    string
		stack   Stacker[testData]
		items   []testData
		want    testData
		wantLen int
		wantErr bool
	}{
		{
			name:    "peek one",
			stack:   NewStack[testData](5),
			items:   []testData{{id: 1}},
			want:    testData{id: 1},
			wantLen: 1,
			wantErr: false,
		},
		{
			name:    "stack is empty",
			stack:   NewStack[testData](5),
			items:   []testData{},
			want:    testData{},
			wantLen: 0,
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				_ = tt.stack.Push(item)
			}

			got, err := tt.stack.Peek()
			if (err != nil) != tt.wantErr {
				t.Errorf("Stack.Peek() error = %v, wantErr %v", err, tt.wantErr)
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Stack.Peek() = %v, want %v", got, tt.want)
			}
			if tt.stack.Len() != tt.wantLen {
				t.Errorf("Stack.Peek() len = %v, want %v", tt.stack.Len(), tt.want)
			}
		})
	}
}

func TestStack_IsFull(t *testing.T) {
	tests := []struct {
		name  string
		stack Stacker[testData]
		items []testData
		want  bool
	}{
		{
			name:  "push one, not full",
			stack: NewStack[testData](5),
			items: []testData{{id: 1}},
			want:  false,
		},
		{
			name:  "null len, not full",
			stack: NewStack[testData](5),
			items: []testData{},
			want:  false,
		},
		{
			name:  "full stack",
			stack: NewStack[testData](3),
			items: []testData{{id: 1}, {id: 2}, {id: 3}},
			want:  true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				_ = tt.stack.Push(item)
			}

			got := tt.stack.IsFull()
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Stack.IsFull() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStack_IsEmpty(t *testing.T) {
	tests := []struct {
		name  string
		stack Stacker[testData]
		items []testData
		want  bool
	}{
		{
			name:  "push one, not empty",
			stack: NewStack[testData](5),
			items: []testData{{id: 1}},
			want:  false,
		},
		{
			name:  "null len, is empty",
			stack: NewStack[testData](5),
			items: []testData{},
			want:  true,
		},
		{
			name:  "full stack, not empty",
			stack: NewStack[testData](3),
			items: []testData{{id: 1}, {id: 2}, {id: 3}},
			want:  false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				_ = tt.stack.Push(item)
			}

			got := tt.stack.IsEmpty()
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Stack.IsEmpty() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStack_Len(t *testing.T) {
	tests := []struct {
		name  string
		stack Stacker[testData]
		items []testData
		want  int
	}{
		{
			name:  "push 1 elem",
			stack: NewStack[testData](5),
			items: []testData{{id: 1}},
			want:  1,
		},
		{
			name:  "null len",
			stack: NewStack[testData](5),
			items: []testData{},
			want:  0,
		},
		{
			name:  "push 3 elem",
			stack: NewStack[testData](3),
			items: []testData{{id: 1}, {id: 2}, {id: 3}},
			want:  3,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			for _, item := range tt.items {
				_ = tt.stack.Push(item)
			}

			got := tt.stack.Len()
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Stack.Len() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStack_Cap(t *testing.T) {
	tests := []struct {
		name  string
		stack Stacker[testData]
		want  int
	}{
		{
			name:  "init zero stack",
			stack: NewStack[testData](0),
			want:  0,
		},
		{
			name:  "init stack with 5 cap",
			stack: NewStack[testData](5),
			want:  5,
		},
		{
			name:  "init stack with 3 cap",
			stack: NewStack[testData](3),
			want:  3,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.stack.Cap()
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Stack.Cap() = %v, want %v", got, tt.want)
			}
		})
	}
}
