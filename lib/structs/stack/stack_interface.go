package stack

const (
	isFull  = "Stack is full"
	isEmpty = "Stack is empty"
)

type Stacker[T any] interface {
	Push(data T) error
	Pop() (T, error)
	Peek() (T, error)
	IsEmpty() bool
	IsFull() bool
	Len() int
	Cap() int
}
