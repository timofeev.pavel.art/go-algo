package queue

type Queuer[T any] interface {
	Push(data T) error
	Pop() (T, error)
	Get() (T, error)
	IsFull() bool
	IsEmpty() bool
	Len() int
	Cap() int
}

type Dequeuer[T any] interface {
	PushFront(data T) error
	PushBack(data T) error
	PopFront() (T, error)
	PopBack() (T, error)
	GetFront() (T, error)
	GetBack() (T, error)
	IsFull() bool
	IsEmpty() bool
	Len() int
	Cap() int
}
