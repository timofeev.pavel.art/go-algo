## Go-algo Project
This project was created in order to learn and study various data structures and algorithms. 
Also, these algorithms and ds, if possible, are created in such a way that they can be reused in other projects where they adre needed.

### Planned
#### Data structure
- [x] Single linked-list
- [x] Double linked-list
- [x] Stack
- [ ] Queue
- [ ] Dequeue
- [ ] Binary Search Tree
- [ ] Graph

#### Algo
- Depth-First Search(Preorder, Inorder, Postorder)
- Breadth-First Search
